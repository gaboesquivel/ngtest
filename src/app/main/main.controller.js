(function() {
  'use strict';

  angular
    .module('18techsNgTest')
    .controller('MainController', MainController);

  /** @ngInject */
  function MainController($timeout, contents) {
    var vm = this;

    vm.contents = [];

    activate();

    function activate() {
      getContents();
    }

    function getContents() {
      vm.contents = contents.getContents();
    }
  }
})();
