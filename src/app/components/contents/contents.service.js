(function() {
  'use strict';

  angular
      .module('18techsNgTest')
      .service('contents', contents);

  /** @ngInject */
  function contents() {
    var data = [
      {
        'title': 'Bacon ipsum dolor amet short loin meatloaf spare ribs sausage.',
        'html': '<p>Bresaola ball tip meatloaf, jerky brisket pancetta picanha pig biltong pork belly. Leberkas alcatra sausage venison shoulder shank tri-tip landjaeger porchetta. Ham bresaola short ribs chuck. Ball tip pork chop frankfurter doner turkey, porchetta pork belly.</p>'
      },
      {
        'title': 'Shankle sausage pancetta',
        'html': '<p>Short loin pork chop turducken ham hock swine ground round bresaola venison pig frankfurter cow meatloaf t-bone picanha brisket. Chicken alcatra chuck, andouille pork belly jowl ribeye.</p>'
      },
      {
        'title': 'Instructions',
        'html': '<ol><li>Using RWD (Responsive Web Design) techniques and the compass breakpoint mixin, show the sections vertically (one above the other) if the display is a handheld or tablet device, but side-by-side with equal widths for desktop user agents. </li><li>Using SASS write a cross browser gradient mixin and use it to apply different gradients to each section.</li><li>Use <a href="http://dsx.weather.com/util/image/w/68a62f4e-122e-4c72-91b2-ec9f5024e031.jpg?api=7db9fe61-7414-47b5-9871-e17d87b8b6a0&amp;h=598&amp;w=640&amp;v=at" target="_blank">http://dsx.weather.com/util/<wbr>image/w/68a62f4e-122e-4c72-<wbr>91b2-ec9f5024e031.jpg?api=<wbr>7db9fe61-7414-47b5-9871-<wbr>e17d87b8b6a0&amp;h=598&amp;w=640&amp;v=at</a> as the image if the device viewing the page is a Retina display.</li><li> Set this page up as an Angular app. Manually initialize the app before the closing body tag.</li><li>Create an angular directive with an interactive element that makes a simple change to the the presentation. For example, click a button and change the background color. Load the directive template from $templateCache. Feel free to be creative or even silly.	</li><li>* Bonus question: Create a factory and use it in your directive to calculate the area of the directive element. Display the result of the calculation.</li></ol>'
      }
    ];

    this.getContents = getContents;

    function getContents() {
      return data;
    }
  }

})();
