/*globals document */
(function() {
    'use strict';

    angular
        .module('18techsNgTest')
        .directive('bgBtn', bgBtn);

    /** @ngInject */
    function bgBtn(area) {

        var directive = {
            restrict: 'E',
            templateUrl: 'app/components/bg-btn/button.html',
            link: linkFunc
        };

        return directive;

        function linkFunc(scope, el) {
          el.on('click', function(){
            document.body.style.background = '#' + Math.random().toString(16).slice(2, 8);
          });

          var elArea = area.calculate(el[0]);
          el.after('<p>button area = '+elArea+'px</p>');
        }
    }
})();
