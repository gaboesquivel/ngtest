(function() {
    'use strict';

    angular
        .module('18techsNgTest')
        .factory('area', area);

    /** @ngInject */
    function area() {
        var service = {
            calculate: calculate
        };

        return service;

        function calculate(el) {
          var rect = el.getBoundingClientRect();
          return rect.height * rect.width;
        }
    }
})();
