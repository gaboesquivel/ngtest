(function() {
  'use strict';

  angular
    .module('18techsNgTest')
    .config(config);

  /** @ngInject */
  function config($logProvider) {
    // Enable log
    $logProvider.debugEnabled(true);
  }

})();
