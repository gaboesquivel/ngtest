(function() {
  'use strict';

  angular
    .module('18techsNgTest', ['ngTouch', 'ngSanitize', 'bhResponsiveImages']);

})();
